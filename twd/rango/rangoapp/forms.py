#coding:utf-8
from django import forms
from .models import Category, Page

class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=128,
                           help_text="Nome da categoria.")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)

    # An inline class to provide additional information on the form.
    class Meta:
        # Provide an association between the ModelForm and a model
        model = Category
        fields = ('name',)

    def clean(self):
        cleaned_data = self.cleaned_data

        url = cleaned_data.get('url')
        print(cleaned_data)
        print(url)

        url = url.lower()
        if url and not url.startswith('http://'):
            url = 'http://' + url
        cleaned_data['url'] = url

        return cleaned_data



class PageForm(forms.ModelForm):
    title = forms.CharField(max_length=128,
                            help_text="Título da página.")
    url = forms.URLField(max_length=200,
        help_text="URL da página.")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    class Meta:
        model = Page
        exclude = ('category',)